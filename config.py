# CONFIG
# 
# Replace the value below with something suitable for your own environment
#
# STOREURL is the URL for your WooCommerce store website
# TIMEOUT should be tuned based on how responsive your website
# PAGESIZE determines how many values are collected from WooCommerce at a time. 
# Maximum value is 100, but if the script keeps timing out while retreiving or 
# writing data, try a smaller value. 
#
# WOOCOMMERCEAPI_CONSUMERKEY and WOOCOMMERCEAPU_CONSUMERSECRET are a pair of
# unique values which you will generate within the WooCommerce admin menus.
#
# XMLFILE is the filename of the XML file supplied by your product supplier
CONFIG = {
        'STOREURL': 'https://pc-builder.co.za',
        'WOOCOMMERCEAPI_CONSUMERKEY': 'ck_XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX',
        'WOOCOMMERCEAPI_CONSUMERSECRET': 'cs_XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX',
        'TIMEOUT': 120,
        'PAGESIZE': 50,
        'XMLFILE': 'raw.xml',





        'END': ''
        }
