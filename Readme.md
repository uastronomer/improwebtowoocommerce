# Product updater for PC Builder
## pc-builder.co.za

pc-builder.co.za is an ecommerce website built on Wordpress with the WooCommerce plugin. This script downloads an XML file containing a full listing of products from one of our suppliers (Esquire Technologies) and is provided by the team providing their internal software (Improweb).

The script reads the product list produced by Improweb, and updates the website's product list to match.

**This script is currently in development. These are the development milestones, which will be highlighted upon completion**

1. **Load the XML as a file**
2. **Parse the XML object using the BeautifulSoup python module, to create a** __list__ **of product objects, with each product stored as a** __dict__.
3. **Connect to the website's XMLRPC interface, using code borrowed from the Conservation Symposium archive updater.**
Change of plan - used official WooCommerce python API instead of messing around with Wordpress RPCXML
4. **Create list of categories, that can be used to map between DataFile category field and WooCommerce's category list.**
5. **Create list of products currently on website, in same format (list of dicts) as earlier**.
6. **Iterate through objects, to create a new Product listing in the website. Delete all existing products before starting.**
7. **Rewrite compareLists() to return fill productLists, and not just lists of SKUs**
8. **Refactor: New function responsible for all WooCommerce connections which will properly handle exceptions and return the results of the wcapi.post command. Maybe def wooConnect(woo, operation, data)? Rewrite all connections to use this function.**
9. **Enhancement: We're including stock numbers when uploading to WooCommerce, but we're not enabling "Manage Stock" so that value is being discarded. Figure out how to turn on "Manage Stock" when uploading or updating a product"**
10. Enhancement: Find a way to tag all products created by this script, so as to not delete manually created products. Bonus points if we recognise a manual product and update price and inventory anyway!
11. Add options for XML source (configured URL, command-line URL, command-line filesystem path)
12. NO MORE BULK DELETES! Change code to **first read entire website product catalogue into a __list__ with each product being stored as a __dict__. Compare existing product list with updated XML list (indexed by SKU/Product Code). If a product already exists, update any changed keys (Price, etc). If it does not exist, create a new product. Products that exist, and were created by this script, but that are no longer on the supplier list, should be deleted**
