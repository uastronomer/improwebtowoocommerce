#!/usr/bin/python3
# Script to update Woocommerce website product list to match products
# listed in the user-supplied XML file

import sys, time, requests
from bs4 import BeautifulSoup as bs
from woocommerce import API

## Configuration
import config
STOREURL = config.CONFIG['STOREURL']
WOOCOMMERCEAPI_CONSUMERKEY = config.CONFIG['WOOCOMMERCEAPI_CONSUMERKEY']
WOOCOMMERCEAPI_CONSUMERSECRET = config.CONFIG['WOOCOMMERCEAPI_CONSUMERSECRET']
TIMEOUT = config.CONFIG['TIMEOUT']
PAGESIZE = config.CONFIG['PAGESIZE']
XMLFILE = config.CONFIG['XMLFILE']
DEFAULT_IMAGE = config.CONFIG['DEFAULT_IMAGE']

# Mapping out product description fields in supplier datafeed and in WooCommerce
PRODUCTFIELDLIST = [
        'ProductCode',
        'ProductName',
        'Category',
        'Price',
        'AvailableQty',
        'ImageURL',
        'ProductSummary',
        'ProductDescription',
        'ProductAttributes',
        'LenghtCM',
        'WidthCM',
        'HeightCM',
        'MassKG'
        ] # 'LenghtCM' is correct - typo in the product feed!

PRODUCTFIELDMAP = {
        'ProductCode': 'sku',
        'ProductName': 'name',
        'Category': 'categories',
        'Price': 'price',
        'AvailableQty': 'stock_quantity',
        'ImageURL': 'images',
        'ProductSummary': 'short_description',
        'ProductDescription': 'description',
        'ProductAttributes': 'attributes',
        'LenghtCM': 'dimensions',
        'WidthCM': 'dimensions',
        'HeightCM': 'dimensions',
        'MassKG': 'weight'
        } # 'attributes', 'length', 'width' and 'height' are not simple fields
          # and need special treatment in getStoreProducts()
          #
          # Category is stored internally as a list of all categories
          # dimensions is stored as a list of three values
          # LenghtCM is split out from first field of dimensions
          # WidthCM is split out from second field of dimensions
          # HeightCM is split out from third field of dimensions



## Parse datafeed
def parseSupplierFeed(xmlFile):
    print("Loading supplier's product list")
    productList = []
    xmlRawData = open(xmlFile).read()
    XMLData = bs(xmlRawData, 'xml')
    productXML = XMLData.find_all('Product')
    #
    # Extract product data to a list
    for product in productXML:
        productField = {}
        for field in PRODUCTFIELDLIST:
            try:
                productField[field] = product.find(field).contents[0]
            except Exception as e:
                print('Aborting: Failed to load key <' + field + '>')
                print(e)
                exit()
        productList.append(productField)
    # Sometimes the supplier's webserver is down and we get an empty xml file. Do not proceed
    if not len(productList):
        print('ABORT: Empty product list provided by supplier. Please investigate and try again later.')
        sys.exit(1)
    #
    return(productList)


## Retrieve data from store website
def getStoreCategories(woo):
    # Builds a dict mapping all product category IDs on WooCommerce to their names.
    categories = {}
    # WooCommerce API only allows us to fetch a maximum of PAGESIZE categories at a time, so we
    # have to grab them in batches
    page = 1
    while True:
        rawCategories = woo.get('products/categories', params={'per_page': PAGESIZE, 'page': page}).json()
        for rawCategory in rawCategories:
            # Provided category names are HTML encoded. We don't want that.
            categories[rawCategory['name'].replace('&amp;', '&')] = rawCategory['id']
        if len(rawCategories) < PAGESIZE:
            break
        else:
            page += 1
    print(str(len(categories)) + ' categories retrieved from WooCommerce')
    return(categories)

def getStoreProducts(woo):
    productList = []
    rawProducts = []
    # WooCommerce APU returns only 10 products at a time, so we have to grab them in batches
    print('Retrieving products from ' + STOREURL)
    page = 1
    while True:
        productBatch = woo.get('products', params={'per_page': PAGESIZE, 'page': page}).json()
        rawProducts = rawProducts + productBatch
        #print('Page: ', page)
        if len(productBatch) < PAGESIZE:
            break
        else: 
            page += 1
    print(str(len(rawProducts)) + ' products retrieved from WooCommerce')
    #For each field in rawProduct, assign it's value to the matching key from the list of PRODUCTFIELDLIST in a dict, then append that dict to productList
    for rawProduct in rawProducts:
        productDict = {}
        for field in PRODUCTFIELDLIST:
            try:
                productDict[field] = rawProduct[PRODUCTFIELDMAP[field]]
            except Exception as e:
                print('Aborting: Failed to load key <' + field + '> which is mapped to WooCommerce field <' + PRODUCTFIELDMAP[field] + '>')
                print(e)
                exit()
        # Deal with special fields: Categories
        categoryList = []
        for i in range(len(productDict['Category'])):
            categoryList.append(productDict['Category'][i]['name'])
        productDict['Category'] = categoryList
        # Deal with special fields: split dimensions out into LenghtCM, WidthCM and HeightCM
        productDict['LenghtCM'] = rawProduct['dimensions']['length']
        productDict['WidthCM'] = rawProduct['dimensions']['width']
        productDict['HeightCM'] = rawProduct['dimensions']['height']
        # Deal with special fields: unique product id
        productDict['id'] = rawProduct['id']
        #
        productList.append(productDict)
    return(productList)

## Update store website
def updateBatch(woo, command, data):
    batchCommand = {command: data}
    #print('Trying to ' + command + ' ' + str(len(data)) + ' products')
    try:
        result = woo.post('products/batch', batchCommand)
    except Exception as e:
        result = str(e)
    return(result)


def createWooCategory(woo, category):
    print('Adding category "' + category + '" to WooCommerce')
    data = {}
    data['name'] = category
    result = woo.post('products/categories', data).json()
    categoryID=result['id']
    return(categoryID)
    
def compareLists(wooProductList, supProductList):
    # Returns three lists, containing product listings that only exist on the 
    # supplier list, of products that only exist on WooCommerce, and products that exist 
    # on both.
    wooProdDict = {}
    supProdDict = {}
    wooUniqueList = []
    supUniqueList = []
    commonProductList = []
    #
    # The two products lists are similar, but not identical. supProdDict lacks the 'id' 
    # field, for example. To make comparisons easier, create two dicts using SKU/Product Code
    # as indexes. WooCommerce does not treat SKU's as unique, but the supplier does, so this
    # should not cause any collisions.
    for product in wooProductList:
        wooProdDict[product['ProductCode']] = product
    for product in supProductList:
        supProdDict[product['ProductCode']] = product
    #
    for productCode in wooProdDict.keys():
        if productCode in supProdDict:
            commonProductList.append(wooProdDict[productCode])
    #
    # All products now listed in commonProductList[] must be removed from
    # wooProdDict{} and supProdDict{}
    for product in commonProductList:
        del wooProdDict[product['ProductCode']]
        del supProdDict[product['ProductCode']]
    #
    # Finally, decompose wooProdDoct{} and supProdDict{} into lists
    for productCode in wooProdDict.keys():
        wooUniqueList.append(wooProdDict[productCode])
    for productCode in supProdDict.keys():
        supUniqueList.append(supProdDict[productCode])
    #
    return(supUniqueList, wooUniqueList, commonProductList)

def removeProducts(woo, productList):
    ## Extract ['id'] from each product in productList, and store in a new list.
    ## Use this list to delete all those products, in a batch update.
    productIDList = []
    subCount = 0
    count = 0
    for product in productList:
        count += 1
        subCount += 1
        productID = product['id'] 
        productIDList.append(productID)
        if subCount >= PAGESIZE:
            updateBatch(woo, 'delete', productIDList)
            subCount = 0
            productIDList = []
    updateBatch(woo, 'delete', productIDList)
    return('Succesfully deleted ' + str(count) + ' products')

def removeOldProducts(woo, wooProductList, supProductList):
    return None

def addNewProducts(woo, supProductList, categoryNamesDict):
    # products[] is not currently used - for now we're uploading/updating products one at
    # a time, but that's slow and puts load on the WooCommerce server. In future we'll update
    # in batches, and products[] is part of the plumbing that we're setting up now in preparation.
    #
    # 1. Convert product attributes to WooCommerce format (reverse of what's done in getStoreProducts()
    products = []
    for i in range(len(supProductList)):
        wooProduct = {'dimensions': {}, 'categories': [], 'images': [], 'images': [], 'manage_stock': True}
        for field in PRODUCTFIELDLIST:
            #print('Field ' + field + ' is of type: ' + str(type(supProductList[i][field])))
            if field == 'Category':
                categoryList = []
                supCategories = supProductList[i]['Category'].split(':')
                for category in supCategories:
                    # Clean up whitespace - no leading, trailing or duplicated spaces
                    categoryName = ' '.join(str(category).split())
                    if categoryName == '':
                        break
                    elif categoryName not in categoryNamesDict:
                        print('Category "' + categoryName + '" NOT in categoryNamesDict.')
                        categoryID = createWooCategory(woo, categoryName)
                        categoryNamesDict[categoryName] = categoryID
                        print('Created category "' + categoryName + '" and ID ' + str(categoryID))
                    else: 
                        categoryID = categoryNamesDict[categoryName]
                    categoryList.append({'id': int(categoryID)})
                    #
                wooProduct['categories'] = categoryList
            elif (field == 'LenghtCM'): 
                wooProduct['dimensions']['length'] = supProductList[i][field]
            elif (field == 'WidthCM'):
                wooProduct['dimensions']['width'] = supProductList[i][field]
            elif (field == 'HeightCM'):
                wooProduct['dimensions']['height'] = supProductList[i][field]
            elif (field == 'AvailableQty'):
                wooProduct['stock_quantity'] = int(supProductList[i][field])
            elif (field == 'ProductAttributes'):
                wooProduct['ProductAttributes'] = ''
            elif (field == 'ImageURL'):
                imageList = []
                imageList.append({'src': supProductList[i][field], 'alt_text': supProductList[i]['ProductName']})
                wooProduct['images'] = imageList
            else:
                wooField = PRODUCTFIELDMAP[field]
                wooProduct[wooField] = supProductList[i][field]
        #Resolve weird price storage
        wooProduct['regular_price'] = wooProduct['price']
        products.append(wooProduct)
    # 2. Clean up dirty or placeholder records
    for product in products:
        if product['images'][0]['src'] == 'Images/NoPic.jpg':
            product['images'][0]['src'] = DEFAULT_IMAGE
        #Check for bad image URLs and replace them with generic image
        imagePing = requests.head(product['images'][0]['src'])
        if imagePing.status_code >= 400:
            print('    Image for ' + product['name'] + ' returns ' + str(imagePing.status_code) + ' - Replacing with Generic Image.')
            product['images'][0]['src'] = DEFAULT_IMAGE
    # 3. push to woo object!
        #uploadWooProduct(woo, wooProduct)
    count = 0
    subCount = 0
    productBatch = []
    for product in products:
        count += 1
        subCount += 1
        productBatch.append(product)
        if subCount >= PAGESIZE:
            updateBatch(woo, 'create', productBatch)
            print('Resting...')
            time.sleep(60)
            subCount = 0
            productBatch = []
    updateBatch(woo, 'create', productBatch)
    # 3. Report
    output = 'Succesfully added ' + str(count) + ' products:\n'
    if count > 0:
        output += 'Product Name'.rjust(42) + 'SKU'.rjust(22) + 'Price'.rjust(10) + '\n'
    for product in productBatch:
        fprice = float(product['price'])
        price = '{:.2f}'.format(fprice)
        output += product['name'][:40].rjust(40) + ' |' + product['sku'][:20].rjust(20) + ' |' + price.rjust(9) + '\n'
    return(output)


def updateExistingProducts(woo, wooProductList, supProductList):
    # Note that, as written, this will only update prices and stock quantities
    # If you need to change this, it shouldn't be hard to add in the extra fields.
    # Refer to addNewProducts() for guidance
    updatedProducts = []
    count = 0
    for wooProduct in wooProductList:
        updatedProduct = {}
        updatedProduct['id'] = wooProduct['id']
        # find updated prices and stock quantities
        for supProduct in supProductList:
            if supProduct['ProductCode'] == wooProduct['ProductCode']:
                # Only update if there's been a change to the stock quantity or price.
                if (str(supProduct['Price']) != str(wooProduct['Price'])) or (str(supProduct['AvailableQty']) != str(wooProduct['AvailableQty'])):
                    updatedProduct['regular_price'] = supProduct['Price']
                    updatedProduct['price'] = supProduct['Price']
                    updatedProduct['stock_quantity'] = int(float(supProduct['AvailableQty']))
                    updatedProducts.append(updatedProduct)
                    count += 1
                    break
                else:
                    break
        if len(updatedProducts) >= PAGESIZE:
            updateBatch(woo, 'update', updatedProducts)
            updatedProducts = []
    updateBatch(woo, 'update', updatedProducts)
    result = 'Updated ' + str(count) + ' products'
    return(result)


# Connect
woo = API(
    url = STOREURL,
    consumer_key = WOOCOMMERCEAPI_CONSUMERKEY,
    consumer_secret = WOOCOMMERCEAPI_CONSUMERSECRET,
    version = 'wc/v3',
    timeout = TIMEOUT
)

## Build supplier and WooCommerce product lists
supProducts = parseSupplierFeed(XMLFILE)
wooProductList = getStoreProducts(woo)
# Separate out products that are on both lists, remove them from the previous lists
newProducts, oldProducts, bothProducts = compareLists(wooProductList, supProducts)
# Fetch all product categories from store
categoryNamesDict = getStoreCategories(woo)
# Delete all products from WooCommerce
#print(removeProducts(woo, wooProductList))
# Delete old products from store
print(removeProducts(woo, oldProducts))
# Add all products to WooCommerce
#output = addNewProducts(woo, supplierProductList, categoryNamesDict)
# Update existing products
print(updateExistingProducts(woo, bothProducts, supProducts))
# Add all new products to WooCommerce
print(addNewProducts(woo, newProducts, categoryNamesDict))
